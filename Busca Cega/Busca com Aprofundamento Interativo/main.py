#!/usr/bin/python
# coding=utf-8

__author__  = 'Rafael Rubenich'
__project__ = 'Busca com Aprofundamento Interativo'
__status__  = 'Production'


graph = {
    'a' : ['b', 'c'],
    'b' : ['d', 'e'],
    'd' : ['h'],
    'e' : ['i', 'j'],
    'c' : ['f', 'g'],
    'f' : ['k']
}


# Iterative deepening depth-first search
def IDDFS(objective, graph):

    fifo = []

    item = fifo.pop

    while fifo:
        if(item in objective):
            print(item + " Encontrado")
            objective.remove(item)
        fifo.append(graph[item])



    print(fifo)

def dfs(graph, start):
    visited, stack = set(), [start]
    while stack:
        vertex = stack.pop()
        if vertex not in visited:
            visited.add(vertex)
            stack.extend(graph[vertex] - visited)
    return visited



IDDFS(['j', 'f'], graph)